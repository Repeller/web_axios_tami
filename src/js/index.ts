import axios, {
    AxiosResponse,
    AxiosError
} from "../../node_modules/axios/index"; // Don't worry about red lines here ...

interface IUser {
    // attributes from https://tamagotchiwebapi.azurewebsites.net/api/User
    Id: number;
    Username: string;
    Password: string;
}
interface IPower {
    // attributes from https://tamagotchiwebapi.azurewebsites.net/api/EventPower
    Id: number;
    Text: string;
    When: Date;
    FkUserId: number;
}
interface IFood {
    // attributes from https://tamagotchiwebapi.azurewebsites.net/api/EventFood
    Id: number;
    Text: string;
    When: Date;
    FkUserId: number;
}
interface IPlay {
    // attributes from https://tamagotchiwebapi.azurewebsites.net/api/EventPlay
    Id: number;
    Text: string;
    When: Date;
    FkUserId: number;
}
interface IStatus {
    // attributes from https://tamagotchiwebapi.azurewebsites.net/api/Status
    Id: number;
    Mood: number;
    Food: number;
    Hp: number;
    FkUserId: number;
    When: Date;
    
}

// the div elements from the htm file
let E_user: HTMLDivElement = <HTMLDivElement>document.getElementById("contentUser");
let E_food: HTMLDivElement = <HTMLDivElement>document.getElementById("contentFoodEvents");
let E_Power: HTMLDivElement = <HTMLDivElement>document.getElementById("contentPowerEvents");
let E_status: HTMLDivElement = <HTMLDivElement>document.getElementById("contentStatusEvents");
let E_play: HTMLDivElement = <HTMLDivElement>document.getElementById("contentPlayEvents");

// users
axios.get<IUser[]>("https://tamagotchiwebapi.azurewebsites.net/api/User")
    .then((response: AxiosResponse<IUser[]>) => {
        let data: IUser[] = response.data;
        E_user.innerHTML = JSON.stringify(data);
        data.forEach(element => {
            console.log(element.Username);
        });
    })
    .catch((error: AxiosError) => {
        console.log(error);
        E_user.innerHTML = error.message;
    });
// power events
axios.get<IPower[]>("https://tamagotchiwebapi.azurewebsites.net/api/EventPower")
    .then((response: AxiosResponse<IPower[]>) => {
        let data: IPower[] = response.data;
        E_Power.innerHTML = JSON.stringify(data);
        data.forEach(element => {
            console.log(element.Text);
        });
    })
    .catch((error: AxiosError) => {
        console.log(error);
        E_user.innerHTML = error.message;
    });
// food events
axios.get<IFood[]>("https://tamagotchiwebapi.azurewebsites.net/api/EventFood")
    .then((response: AxiosResponse<IFood[]>) => {
        let data: IFood[] = response.data;
        E_food.innerHTML = JSON.stringify(data);
        data.forEach(element => {
            console.log(element.Text);
        });
    })
    .catch((error: AxiosError) => {
        console.log(error);
        E_user.innerHTML = error.message;
    });
// status events
axios.get<IStatus[]>("https://tamagotchiwebapi.azurewebsites.net/api/Status")
    .then((response: AxiosResponse<IStatus[]>) => {
        let data: IStatus[] = response.data;
        E_status.innerHTML = JSON.stringify(data);
        data.forEach(element => {
            console.log(element.When);
        });
    })
    .catch((error: AxiosError) => {
        console.log(error);
        E_user.innerHTML = error.message;
    });
// play events
axios.get<IPlay[]>("https://tamagotchiwebapi.azurewebsites.net/api/EventPlay")
    .then((response: AxiosResponse<IPlay[]>) => {
        let data: IPlay[] = response.data;
        E_play.innerHTML = JSON.stringify(data);
        data.forEach(element => {
            console.log(element.Id);
        });
    })
    .catch((error: AxiosError) => {
        console.log(error);
        E_user.innerHTML = error.message;
    });